---
title: quoted-to-read-escape - CLI tool for transcoding shell escaping
bindings: lib.yaml
functions: lib.bash
template: bash
...

# Escaping text

Bash has a few options for escaping text.

There's [printf][]'s `%q` specifier for quoting a fixed number of arguments,
e.g. `printf "%q %q" "foo bar" "b\\az"`.

Or if escaping a single variable or array,
the [parameter expansion][] `@` specifier with the `Q` operator,
e.g. `a=("foo bar" "b\\az"); echo "${a@Q}"`.

However the intention is to produce a string that "may be reused as input",
and the way it is expected to use that input is via eval, e.g.:

```bash
s="$(printf "%q %q" "foo bar" "b\\az")"
eval echo "$s"
```

which while is reasonably safe for reusing parameters,
since we are responsible for producing that string in the first place,
it is of little use for handling input that was provided as a quoted string.

An alternative exists in the [read][] builtin command,
which has a simple escaping mechanism of `\` escape any `\` characters,
field separator characters, or delimiter characters.

However, writing a subplot statement of `WHEN the user runs foo\ bar b\\az`
is going to be harder to read and maintain than if the command
were readable as a shell command.

~~~scenario
GIVEN an installed quoted-to-read-escape
WHEN I run quoted-to-read-escape \"foo bar\" \"b\\az\"
THEN stdout is exactly foo\ bar b\\az
~~~

# Use in Scenarios

So it would now be possible to implement

~~~~{#test.md .file .markdown .numberLines}
---
title: Test scenario
bindings: test.yaml
functions: test.bash
template: bash
...

# Test

```scenario
WHEN I run echo "foo bar" "b\\az"
THEN stdout contains foo bar b\az
```
~~~~

~~~{#test.yaml .file .yaml .numberLines}
- when: I run (?P<cmd>.*)
  regex: true
  function: run
- then: stdout contains (?P<output>.*)
  regex: true
  function: stdout_contains
~~~

~~~{#test.bash .file .bash .numberLines}
run() {
	read -a args <<<"$(quoted-to-read-escape "$(cap_get cmd)")"
	if "${args[@]}" </dev/null >stdout 2>stderr
	then
		ctx_set exit 0
	else
		ctx_set exit "$?"
	fi
	ctx_set stdout "$(cat stdout)"
	ctx_set stderr "$(cat stderr)"
}

stdout_contains() {
	assert_eq "$(ctx_get stdout)" "$(cap_get output)"
}
~~~

~~~scenario
GIVEN an installed quoted-to-read-escape
AND file test.md
AND file test.yaml
AND file test.bash
AND subplot is installed
WHEN I run subplot codegen test.md -o test.run
THEN command is successful
WHEN I run bash ./test.run --env PATH
THEN command is successful
~~~

[printf]: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-printf
[parameter expansion]: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Shell-Parameter-Expansion
[read]: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-read
