---
title: Mocknet - Ergonomic CLI tools for making test suites for network services
bindings: lib.yaml
functions: lib.bash
template: bash
...

Fakenet is a suite of CLI tools for creating mock networks
with ergonomics geared towards making a test suite for a network service
as easily as writing a one-off shell script by copy-pasting from the internet,
unreservedly using Linux-specific APIs where it provides benefit.

# Roadmap

This is all the needed tools.
The first port of call is to identify if something usable exists.

~~~roadmap
testsession:
  label: >
    test-session

    Runs daemons in a namespaced scope
    that we can refer to by name to check state or kill
    and contrives to guarantee that all are killed at test end.

mocknet:
  status: blocked
  label: >
    mocknet

    Creates network namespaces to represent mock networks
    as a replacement for `ip netns add`, `ip netns exec`
    and `ip link set netns`.
  depends:
  - testsession

mocknetigd:
  status: blocked
  label: >
    mocknet-igd

    Mock service that owns a virtual ethernet bridge
    providing an UPnP-IGD/NAT-PMP/PCP interface
    that changes firewall rules to mock internet port forwarding.
  depends:
  - mocknet

mocknetdns:
  status: blocked
  label: >
    mocknet-dns

    DNS server with standard API
  depends:
  - mocknet

mocknetca:
  status: blocked
  label: >
    mocknet-ca

    Utility for generating a certification authority,
    provides PEM files for CURL_CA_BUNDLE etc
    and server certificates to test HTTPS servers.
  depends:
  - mocknet

mocknetacme:
  status: blocked
  label: >
    mocknet-acme

    An implementation of an ACME server
    to test servers that would use letsencrypt.
  depends:
  - mocknetdns
  - mocknetca

autodnsclient:
  status: blocked
  label: >
    autodns-client

    Tool that automagically, given DNS config and the (addr, port) of a service
    configures the router to forward to it,
    configures a name for and gets a certificate for the externally known address.
  depends:
  - mocknetdns
  - mocknetigd

autoforwarderserver:
  status: blocked
  label: >
    autoforwarder-server

    Server that attests ownership of a domain and a service in-line with ACMEv2
    and configures HTTP request forwarding to the configured URL.
  depends:
  - mocknetdns
  - mocknetacme

autoforwarderclient:
  status: blocked
  label: >
    autoforwarder-client

    Builds on autodnsclient to attest ownership to a configured forwarder server
    and renew as-necessary.
  depends:
  - autodnsclient
  - autoforwarderserver

matrixautodelegateserver:
  status: blocked
  label: >
    matrix-autodelegate-server

    Builds on autoforwarder-server to provide /.well-known/matrix endpoint.
  depends:
  - autoforwarderserver

matrixautodelegateclient:
  status: blocked
  label: >
    matrix-autodelegate-client

    Builds on autoforwarderclient to configure matrix endpoint.
  depends:
  - autoforwarderclient
  - matrixautodelegateserver

matrixtestcli:
  status: blocked
  label: >
    matrix-test-cli

    CLI tools to allow starting background matrix clients
    that connect to virtual matrix servers,
    allow batch control the client to join rooms, send and receive messages.
  depends:
  - matrixautodelegateserver
  - matrixautodelegateclient
~~~

# Command-line interface conventions

1.  Wherever possible extensions to shell behaviour are handled in commands
    rather than making use of shell-specific features or syntax.
    So backgrounding processes with & is avoided
    and identifiers are allocated by the caller where possible
    rather than parsing returned values.
2.  Service availability is handled by launchers synchronising
    rather than polling for readiness.
    So & backgrounding can't be used because it returns flow control
    as soon as it forks
    so the only guaranteed resource to be available is the PID.
3.  Prefer state changing over continuation.
    `pg_virtualenv` or `dbus-run-session` is a continuation style,
    which runs a subprocess with environment variables set to point to the DB
    and tears it down after the subprocess exits.
    This can be hard to use without structuring your entire script around it,
    and making test assertions in such a style can be difficult.
    `cd` or `dbus-launch` are state-changing.
    `dbus-launch` also starts a background process and returns the address,
    which makes it not ideal for parameters.
4.  Session token or configuration should be implicitly passed to commands.
    e.g. DBUS_SESSION_BUS_ADDRESS is set in the environment
    rather than passing `--bus` to all commands.
    This may permit deviation of avoiding shell-specific syntax
    to set environment variables in the process.
    It would be preferable to make things entirely implicit.
5.  Daemons handle SIGTERM to clean up before shutting down.
    This is often handled incorrectly,
    one must not only handle the signal to instigate cleanup,
    but also reset the handler and reraise the signal rather than exiting
    for the wait to complete correctly.
6.  Daemons don't have to write a pidfile,
    since they should be run in a cgroup
    because daemons may have multiple processes and PIDs are unreliable,
    but if a pidfile is written it must be locked by the process.

# Test Session

## Containing processes

If all you wanted was to clean up stray processes
then a hashbang line of
`#/usr/bin/env -S systemd-run --user -Pd --wait --service-type=exec sh`
will ensure a subprocess isn't left behind.

However we might have multiple tests in the same script
and want to shut down our services between tests.
Handling this with a subprocess per test makes it harder to use variables.
So instead we will manage this with a subprocess.

### Containing for the lifetime of the launcher

`testsess launch` starts a background process
and outputs the address of the unix socket to communicate with it.

~~~scenario
GIVEN a subprocess
WHEN the subprocess runs testsess launch
THEN the subprocess command exits with code 0
AND the subprocess command output contains DBUS_SESSION_BUS_ADDRESS
~~~

The output is of the form `export FOO=var` such that it may be evaluated
in the same way as `dbus-launch` to set the launcher's environment.

~~~scenario
WHEN the subprocess evaluates the last command output
THEN the subprocess environment contains DBUS_SESSION_BUS_ADDRESS
~~~

Subprocesses started by the process that launches testsess
are cleaned up automatically when the launching process exits.

~~~scenario
WHEN the subprocess runs dummy-daemon --pid-file dummy.pid
THEN the subprocess command exits with code 0
AND the file named dummy.pid exists
AND the file named dummy.pid is locked
WHEN the subprocess exits
THEN the file named dummy.pid becomes unlocked
~~~

### Containing with an explicit end

testsession can be shut-down gracefully by running `testsess shutdown`.

~~~scenario
WHEN the user runs testsess launch
THEN the command exits with code 0
AND the command standard output contains DBUS_SESSION_BUS_ADDRESS
WHEN the user evaluates the last command output
THEN the environment contains DBUS_SESSION_BUS_ADDRESS
WHEN the subprocess runs dummy-daemon --pid-file dummy.pid
THEN the subprocess command exits with code 0
AND the file named dummy.pid exists
AND the file named dummy.pid is locked
WHEN the user runs testsess shutdown
THEN the file named dummy.pid becomes unlocked
~~~

The distinction between graceful and ungraceful shutdown is
that graceful stops accepting new requests but finishes ongoing ones.

Given the semantics are that any subprocesses get cleaned up on shutdown
no special handling is needed compared to SIGTERM,
but the PID of the service is not exposed to do so.

## Service management

You can start a daemon and let it run in the background,
and it will be cleaned up on scope exit,
but you may have cause to selectively stop or restart services.

The traditional way to handle this is by using a pid-file.
Done properly by locking the file to assert the PID is valid
allows you to safely assert it's still running or kill it,
but still has a couple of problems:

1.  You can't safely and reliably assert it's been stopped
    since if it's not running the PID is invalid to use and may have been recycled
    giving a false-negative.
2.  A pid-file only lists one PID, so a service that uses subprocesses
    can't be reliably cleaned up.

`systemd-run --user --same-dir --service-type=forking` will start a service,
exit when it has started, and report a generated service name
that can then be used to kill the service.

However it is inconvenient to parse the service name from the output of that
and it's either not safe or not reliable to define the service name.

### Explicitly managed services

`testsess` provides the `start` subcommand to start and name a daemon.

~~~scenario
GIVEN testsess is running
WHEN the user runs testsess start dummy dummy-daemon --pid-file dummy.pid
THEN the command exits with code 0
AND the file named dummy.pid exists
AND the file named dummy.pid is locked
~~~

It also provides `is-running` and `is-dead` commands to check without a pidfile.

~~~scenario
WHEN the user runs testsess is-running dummy
THEN the command exits with code 0
WHEN the user runs testsess is-dead dummy
THEN the command exits with code 1
~~~

Services may be stopped explicitly with the `stop` command.

~~~scenario
WHEN the user runs testsess stop dummy
THEN the command exits with code 0
AND the file named dummy.pid becomes unlocked
WHEN the user runs testsess is-running dummy
THEN the command exits with code 1
WHEN the user runs testsess is-dead dummy
THEN the command exits with code 0
~~~

### Automatic cleanup of services

`testsess shutdown` stops any running services directly
rather than allowing them to end on scope exit,
since the scope only exits when all processes exit
including the parent process that is supposed to outlive shutdown.

~~~scenario
GIVEN a subprocess
WHEN the subprocess runs testsess launch
AND the subprocess evaluates the last command output
AND the subprocess runs dummy-daemon --pid-file dummy.pid
AND the subprocess exits
THEN the file named dummy.pid becomes unlocked
~~~

# `mocknet`
## `mocknet launch` and `mocknet shutdown`

TODO: Is shutdown needed?
That's graceful exit by rejecting new requests
but completing ongoing requests before shutting down.
mocknet should cope with cleaning up on terminate,
and should only be started under management that guarantees it.

Mocknet is run as a background service that creates a socket to receive commands
and detaches from the launching terminal,
exiting the launcher process only after the socket is ready to receive requests.

```scenario
WHEN the user runs testsess start mocknet launch
THEN the command exits with code 0
WHEN the user runs mocknet shutdown
THEN the command exits with code 0
```

# Glossary

## CLI

Command-line interface. A CLI tool is a tool run in a CLI.

## PID

Process Identifier, unique within the current process namespace,
valid from between the fork that returns it until it is reaped.

## PIDFD

Special file descriptor that refers to a process.

## pidfile

A locked regular file whose contents is the PID of a running process.
