use std::path::PathBuf;

use anyhow::Result;
use libc::pause;
use structopt::StructOpt;

use cli_test_toolkit::background::{background, default_ready, default_setup};

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(long)]
    pid_file: Option<PathBuf>,
}

#[paw::main]
fn main(args: Args) -> Result<()> {
    background(default_setup, default_ready, args.pid_file)?;
    // As dummy daemon, suspend execution with pause
    // SAFETY: There are no threads depending on this to do anything.
    unsafe { pause() };
    Ok(())
}
