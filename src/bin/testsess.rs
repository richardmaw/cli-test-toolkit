use core::convert::{TryFrom, TryInto};
use std::{
    mem::size_of, os::raw::c_long, path::PathBuf, process::Command,
    str::from_utf8,
};

use libc::pid_t;
use serde::{Deserialize, Serialize};
use shell_words::quote;
use structopt::StructOpt;
use thiserror::Error;

use cli_test_toolkit::background::{background, ReadyError};

#[derive(Debug, StructOpt)]
struct LaunchArgs {}

#[derive(Debug, StructOpt)]
enum Action {
    Launch(LaunchArgs),
}

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(subcommand)]
    action: Action,
}

#[derive(Clone, Debug, Deserialize, Error, Serialize)]
enum DBusLaunchError {
    #[error("Subprocess spawn failed: {0}")]
    SpawnError(String),
    #[error("Subprocess exited {0:?} with message {1:?}")]
    CommandError(Option<i32>, Vec<u8>),
}

fn dbus_launch() -> Result<Vec<u8>, DBusLaunchError> {
    // TODO: This uses the default session config, we may want different filter
    // or to put the socket somewhere other than /tmp
    let output = Command::new("dbus-launch")
        .arg("--binary-syntax")
        .output()
        .map_err(|e| DBusLaunchError::SpawnError(e.to_string()))?;
    if !output.status.success() {
        return Err(DBusLaunchError::CommandError(
            output.status.code(),
            output.stderr,
        ));
    }
    Ok(output.stdout)
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct DBusSessionParameters {
    address: Vec<u8>,
    pid: pid_t,
    x_window_id: c_long,
}

#[derive(Clone, Debug, Deserialize, Error, Serialize)]
enum DBusSessionParametersParseError {
    #[error("Truncated address")]
    TruncatedAddress,
    #[error("Truncated PID")]
    TruncatedPID,
    #[error("Truncated X Window ID")]
    TruncatedXWID,
    #[error("Unrecognised trailing data")]
    TrailingData,
}

// TODO: testsess is 3 things.
// 1.  d-bus daemon scoped to parent process lifetime
// 2.  systemd scope for parent process lifetime
// 3.  proxy service for starting services
//
// Can simplify with doing just #2,
// then launching #1
// and having #3 take options for which session bus to serve on
// and connect to default,
// by having #3 handle SIGTERM by killing any services
// ```bash
// dbusopts="$(dbus-launch)"
// new_session="$(eval "$dbusopts" && echo "$DBUS_SESSION_ADDRESS")"
// daemon_pid="$(eval "$dbusopts" && echo "$DBUS_SESSION_BUS_PID")"
// # and in addition to adding parent pid (argument defaults to getppid)
// # adds listed PIDs to the scope and on parent PID exit
// # sends sigterm, sigcont, waits with 3 second timeout, then SIGKILL
// # first to everything not previously listed from the scope
// # and then PIDs in reverse order listed.
// scope-procs launch --serve-address "$new_session" --pid=$$ \
//          --kill-signal=SIGTERM --no-send-sighup --timeout-stop=3 \
//          --final-kill-signal=SIGKILL -- "$daemon_pid"
//
// # Runs in the background, doesn't need a PID file because pidscope will terminate it
// testsvc launch --serve-address "$new_session"
// eval "$dbusopts"
// # `testsvc start NAME CMD` will connect to bus and send a message to testsvc to start service
// # `testsvc stop NAME` will connect to bus and send a message to testsvc to stop service
// testsvc shutdown # stops running test services
// pidscope shutdown # explicit shutdown, terminates processes in order
// ```

impl TryFrom<Vec<u8>> for DBusSessionParameters {
    type Error = DBusSessionParametersParseError;
    fn try_from(mut bytes: Vec<u8>) -> Result<Self, Self::Error> {
        let idx = match bytes.iter().position(|&v| v == 0) {
            Some(idx) => idx,
            None => {
                return Err(DBusSessionParametersParseError::TruncatedAddress)
            }
        };
        let bytes_slice = &bytes[idx + 1..];

        let pid_slice = match bytes_slice.get(..size_of::<pid_t>()) {
            Some(pid_slice) => pid_slice,
            None => return Err(DBusSessionParametersParseError::TruncatedPID),
        };
        let pid_array = pid_slice
            .try_into()
            .expect("from_ne_bytes length same as size_of");
        let pid = pid_t::from_ne_bytes(pid_array);
        let bytes_slice = &bytes_slice[size_of::<pid_t>()..];

        let xwid_array = match bytes_slice.try_into() {
            Ok(xwid_array) => xwid_array,
            Err(_e) => {
                return Err(DBusSessionParametersParseError::TruncatedXWID)
            }
        };
        let x_window_id = c_long::from_ne_bytes(xwid_array);
        let bytes_slice = &bytes_slice[size_of::<c_long>()..];

        if bytes_slice.len() != 0 {
            return Err(DBusSessionParametersParseError::TrailingData);
        }

        bytes.truncate(idx);
        let address = bytes;

        Ok(DBusSessionParameters {
            address,
            pid,
            x_window_id,
        })
    }
}

#[derive(Clone, Debug, Deserialize, Error, Serialize)]
enum LaunchError {
    #[error("dbus-launch failed: {0}")]
    DBusLaunchFailure(#[from] DBusLaunchError),
    #[error("Session parameter parse from dbus-launch failed: {0}")]
    SessionParameterParseFailure(#[from] DBusSessionParametersParseError),
}

fn launch(_args: LaunchArgs) -> anyhow::Result<()> {
    let setup = || {
        let output = dbus_launch()?;
        Ok((DBusSessionParameters::try_from(output)?, ()))
        // TODO: Make PIDFD of parent process
        // TODO: Add selector of PIDFD that kills scope on exit
        // TODO: Connect to session bus
        // TODO: Create scope of this, DBus and parent process
        // TODO: Start proxy bus service
        // TODO: Return that context
    };
    let ready =
        |res: Result<DBusSessionParameters, ReadyError<LaunchError>>| {
            match res {
                Err(e) => eprintln!("{}", e),
                Ok(dbus_session_parameters) => {
                    let address = from_utf8(&dbus_session_parameters.address)
                        .expect("session address should be utf8 string");
                    // TODO: Support other formats based on args
                    print!(
                        "DBUS_SESSION_ADDRESS={};\n\
                         export DBUS_SESSION_ADDRESS;\n\
                         DBUS_SESSION_BUS_PID={};\n",
                        quote(address),
                        dbus_session_parameters.pid,
                    );
                }
            }
        };
    background(setup, ready, None::<PathBuf>)?;
    Ok(())
}

#[paw::main]
fn main(args: Args) -> anyhow::Result<()> {
    match args.action {
        Action::Launch(launch_args) => launch(launch_args),
    }
}
