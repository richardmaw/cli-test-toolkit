use core::fmt::Debug;
use std::{
    fs::File,
    io::Read,
    os::unix::io::AsRawFd,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, Context};
use nix::{
    errno::Errno,
    fcntl::{flock, FlockArg},
    sys::signal::{kill, Signal},
    unistd::Pid,
};

use cli_test_toolkit::scope_procs::AsyncScopeProcsProxy;

pub(crate) enum KillSuccess {
    Shutdown,
    Killed,
}

fn do_shutdown() -> anyhow::Result<()> {
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_io()
        .build()?;
    runtime.block_on(async {
        let dbus = zbus::azync::Connection::session().await?;
        let ticker = {
            let dbus = dbus.clone();
            async move {
                loop {
                    dbus.executor().tick().await;
                }
            }
        };
        let shutdown = async {
            let scope_procs = AsyncScopeProcsProxy::new(&dbus).await?;
            scope_procs.shutdown().await?;
            Ok(())
        };
        tokio::select! {
            _ = ticker => unreachable!(),
            res = shutdown => res,
        }
    })
}

fn do_kill<P>(pid_file: P) -> anyhow::Result<()>
where
    P: AsRef<Path> + Debug,
{
    let mut f: File = File::open(&pid_file)
        .with_context(|| format!("Failed to open PID file {:?}", pid_file))?;
    match flock(f.as_raw_fd(), FlockArg::LockSharedNonblock) {
        Err(Errno::EWOULDBLOCK) => (),
        Err(e) => return Err(anyhow!("flock failed: {:?}", e)),
        Ok(()) => return Err(anyhow!("Watcher not running")),
    }
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    let pid: Pid = Pid::from_raw(s.parse()?);
    kill(pid, Signal::SIGTERM)?;
    match kill(pid, Signal::SIGCONT) {
        Err(Errno::ESRCH) => Ok(()),
        res => res,
    }?;
    Ok(())
}

pub(crate) fn kill_watcher(pid_file: PathBuf) -> anyhow::Result<KillSuccess> {
    if do_shutdown().is_ok() {
        return Ok(KillSuccess::Shutdown);
    }
    do_kill(&pid_file).map(|_| KillSuccess::Killed)
}
