---
title: Scope Proc - CLI tool for moving the current process into a systemd scope
bindings: lib.yaml
functions: lib.bash
template: bash
...

It's very impolite when a script leaves background processes running.

The traditional UNIX approach is to have a [session][]
with a controlling TTY,
and kill the whole session with `SIGHUP` when the TTY logs out
(which is overridable with [nohup][]).

This is both hard to integrate into an existing script
and background processes may escape by creating their own session
(which is desirable in traditional UNIX as it is how you make daemons).

[cgroups][] were invented in part to address this,
but architectural issues make it unsafe to have multiple cgroup trees
so they are under the management of systemd.

The user-interface that was designed to be easily controlled via shell scripts
is unavailable to them, instead it is mediated by the [systemd D-Bus API][]
which re-exports the feature under the name [scopes][].

The [systemd-run][] command exists to partially address this,
and provides an API to start commands in a [cgroup][cgroups], however:

1.  Its output is designed to be human readable rather than shell-parseable.
2.  It does not provide an API for adding an existing process to a scope.

Command-line D-Bus interfaces exist for making requests,
however the [systemd D-Bus API][] does not provide a convenient interface,
since its API for creating [scopes][] returns a Job ID
which the caller is expected to watch for JobRemoved signals for the result.

>   It is in fact complicated further by the interface.
>   The signal watcher must be registered before creating the scope,
>   but not poll the D-Bus socket for events until it has parsed the result
>   of creating the scope, so it can know which signal to watch for.
>   This precludes any multi-threading of the executor
>   or the handler has to record every event until the known one is given
>   and require unbounded memory,
>   unless there's an API to pause handling the events.
>   This seems to be a known pattern, hence the API exposed by zbus
>   expects calling `while proxy.next_signal().await?.is_some() {}`.

Logic to handle the JobRemoved signal is too specific to make a generic signal
listening CLI tool, so there's a niche for a specific tool.

The goal is that we can have a tool where the minimum we need to do is
have a script that starts:

~~~sh
#!/bin/sh
scope-procs
~~~

and then all subprocesses are automatically contained.

However, a subprocess can't wait for its parent to exit
and pidfd access can't be guaranteed,
which leaves the need for a fallback API.

The "pipe trick" can be used in bash scripts
by use of a fifo, which makes the API become:

~~~sh
#!/bin/sh
if fifo="$(target/debug/scope-procs)"
then
    exec {pipefd}>"$fifo"
    enable -f /usr/lib/bash/fdflags fdflags
    fdflags -s+cloexec "$pipefd"
else
    exit 1
fi
~~~

>   This is not a nice API to use.
>   The parent process' cooperation is unavoidable.
>   The background process MUST open the read end of the pipe first,
>   since that is the only end that can be opened without blocking first.
>   The pipe may be created by the parent and passed in,
>   rather than returned out, but that's not really an improvement.
>   Not being able to have non-parents await a process hampers the utility
>   of tracking by process-id.
>   The session-scope lifetime is now that of the file descriptor,
>   and the launcher returns the path since:
>   1.  It must only be opened after the background process has
>   2.  The parent then doesn't have to know how paths are generated.
>   We may as well make a tempfile with a random name for the fifo.
>   *HOWEVER* the previous clean shutdown logic assumed waitid.
>   In order for a non-parent to be able to have it shut-down,
>   it needs to expose an API that can be waited on.
>   Shutting down is an inherently bidirectional communication operation,
>   so a pipe is not sufficient.
>   A socketpair would be ideal but there's no way to provide that.
>   We can't even create one by having the parent open a unix socket
>   and the server removing it from the fs on first connection,
>   since bash can only open and it needs a connect call.
>   There _are_ initially fruitful comms channels,
>   the stdout of the launcher can be sent to from the background,
>   but we use the exit of that to signal it's ready.
>   **Specifically** that the output is complete.
>   A `coproc` may be usable, but makes it even more bash specific,
>   when other shells can at least run subprocesses with the equivalent of
>   `${pipefd}>&-` when launching each subprocess.
>   Plus, it's not entirely clear whether coprocesses outlive their parent.
>   Given all this, it sounds like we need an additional channel.
>   Before the launcher exits a control socket is also created
>   and the `--shutdown` command connects to it.
>   DBus _would_ be ideal, but that causes a dependency loop.
>   TODO: is it:
>   ```
>   csock="$(scope-procs)"
>   exec {pipefd}>"$(scope-procs --start "$csock")"
>   scope-procs --kill "$csock"
>   ```
>   ?
>   How does kill locate the control socket?
>   Is there a way to pass pipefd to the kill command
>   and have it able to locate the control socket that way?
>   Is there a relationship between the open file's inode and the dentry?
>   Yes, they have the same inode and device type.
>   **HOWEVER** this requires turning off cloexec which is unergonomic,
>   and needs a default directory to look in anyway.
>   So **EITHER** the returned result from scope-procs is advisory
>   or the output of scope-procs needs to be usable to locate the socket.
>   Given the need for the parent to read a path,
>   we may as well make it mandatory.
>   However then the API becomes:
>   ```
>   csock="$(scope-procs launch)"
>   pipe="$(scope-procs start "$csock")"
>   exec {pipefd}>"$pipe"
>   scope-procs kill "$csock"
>   ```
>   Given there will later be the need to start a D-Bus daemon too,
>   it may start to make sense to consolidate and make the socket implicit.
>   ```
>   test_session() {
>       # TODO: This should go in a .profile snippet
>       # TODO: Just bundle all this up into testd and testctl
>       local busopts busaddr buspid pipe
>       busopts="$(dbus-launch)"
>       busaddr="$(eval "$busopts" && echo "$DBUS_SESSION_BUS_ADDRESS")"
>       buspid="$(eval "$busopts" && echo "$DBUS_SESSION_BUS_PID")"
>       pipe="$(scope-procsd --serve-bus "$busaddr" -- "$buspid")"
>       exec {PIPEFD}>"$pipe"
>       enable -f /usr/lib/bash/fdflags fdflags
>       fdflags -s+cloexec "$PIPEFD"
>   
>       testd --serve-bus "$busaddr"
>   
>       OLDBUSADDR="$DBUS_SESSION_BUS_ADDRESS"
>       DBUS_SESSION_BUS_ADDRESS="$busaddr"
>       end_test_session() {
>           scope-procs kill
>           exec "$PIPEFD">&-
>           DBUS_SESSION_BUS_ADDRES="$OLDBUSADDR"
>           unset end_test_session
>       }
>   }
>   test_session
>   testctl start mocknet mocknetd
>   testctl stop mocknet
>   end_test_session

[session]: https://man7.org/linux/man-pages/man2/setsid.2.html
[nohup]: https://man7.org/linux/man-pages/man1/nohup.1p.html
[cgroups]: https://man7.org/linux/man-pages/man7/cgroups.7.html
[systemd D-Bus API]: https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html
[scopes]: https://www.freedesktop.org/software/systemd/man/systemd.scope.html#
[systemd-run]: https://www.freedesktop.org/software/systemd/man/systemd-run.html#

# `scope-procs`

## Implicit default addresses

For the ideal command-line interface there should be an implicit default address
to connect to and send control commands.

For example, D-Bus uses `/run/user/$(id -u)/bus`,
since the UID is a unique identifier for the session bus
given a user can only have one user session.

One goal is to be usable in test suites,
which means it has to not have any namespace clashes
with any services the user might have running anyway.

One option would be to use a per-test D-Bus daemon,
though since the D-Bus daemon should also be part of the scope
this causes a bootstrapping problem.
This problem _is_ solvable by allowing the `scope-procs` command
to be given additional PIDs of processes to add to the scope,
but it would be cleaner to not need that.

A test-suite's PID is a unique identifier while it is running
for the PID namespace it is in.
This does not risk clashing with a user's running service
as a D-Bus daemon would
since the PID is not sharable with another process the user is running.

It is possible for processes in different PID namespaces
to share the same filesystem mount namespace
and hence share the same `/run/user` mount,
so the unique identifier should include the namespace ID.

Since the only command that needs to be sent
is an instruction to stop without waiting for process exit
the implicit default address can point to a PID file.

~~~scenario
GIVEN a systemd-user session is available
AND a session d-bus is available
AND an installed quoted-to-read-escape
AND an installed scope-procs
WHEN I run scope-procs
THEN the command exits with code 0
AND a directory called scope-procs exists in the user's runtime directory
AND a pidfile with name matching PIDNS-PPID.pid exists in the scope-procs runtime directory
~~~

For convenience of not having to parse the namespace, `scope-procs` has a helper
to kill the process.
This performs a clean shutdown that doesn't leave the PID file behind.

~~~scenario
WHEN I run scope-procs --kill
THEN there is no pidfile with name matching PIDNS-PPID.pid in the scope-procs runtime directory
~~~

## Killing subprocesses on process end

As-specified, when the process exits any subprocesses are cleaned up.

~~~scenario
GIVEN a systemd-user session is available
AND a session d-bus is available
AND an installed quoted-to-read-escape
AND an installed scope-procs
AND a subprocess
WHEN the subprocess runs scope-procs
THEN the command exits with code 0
WHEN the subprocess runs dummy-daemon --pid-file dummy.pid
THEN the command exits with code 0
AND the file named dummy.pid exists
AND the file named dummy.pid is locked
WHEN the subprocess exits
THEN the file named dummy.pid becomes unlocked
~~~

## Killing subprocesses on explicit end

In addition to explicit end cleaning itself up,
any subprocesses are also terminated.

~~~scenario
GIVEN a systemd-user session is available
AND a session d-bus is available
AND an installed quoted-to-read-escape
AND an installed scope-procs
WHEN I run scope-procs
THEN the command exits with code 0
WHEN I run dummy-daemon --pid-file dummy.pid
THEN the command exits with code 0
AND the file named dummy.pid exists
AND the file named dummy.pid is locked
WHEN I run scope-procs --kill
THEN the file named dummy.pid becomes unlocked
~~~
